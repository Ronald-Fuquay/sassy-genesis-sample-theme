# Genesis Sample Theme with SCSS 

Github project link: https://Ronald-Fuquay@bitbucket.org/Ronald-Fuquay/sassy-genesis-sample-theme.git


## Installation Instructions

1. Upload the Genesis Sample theme with SCSS folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Genesis Sample theme with SCSS.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.


## Theme Support

Please visit http://my.studiopress.com/help/ for theme support.